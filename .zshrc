# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="af-magic"

DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

export CLICOLOR=1
alias vim=nvim

alias ls='ls --group-directories-first --color=auto'
alias la='ls -lAh --group-directories-first --color=auto'
alias ll='ls -lh --group-directories-first --color=auto'

alias f="cd ~ && cd \$(find * -type d | fzf)"

alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias penv='source $(select-venv)'
export PATH="$PATH:/home/alex/.local/share/coursier/bin"
export PATH="$PATH:/home/alex/.local/bin"
export CARGO_NET_GIT_FETCH_WITH_CLI=true
export ANNO_DIR=/home/alex/kognic/repos/annotell
xset r rate 200 25

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/alex/.local/bin/google-cloud-sdk/path.zsh.inc' ]; then . '/home/alex/.local/bin/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/alex/.local/bin/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/alex/.local/bin/google-cloud-sdk/completion.zsh.inc'; fi

source <(fzf --zsh)
HISTFILE=~/.zsh_history
HISTSIZE=10000
setopt appendhistory


