local handle = io.popen("which python")
local result = handle:read("*a")
handle:close()

require("dap-python").setup(result)
require("dap-python").test_runner = "pytest"

vim.keymap.set("n", "<leader>t", ":lua require('dap-python').test_method()<CR>")
