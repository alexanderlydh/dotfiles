function prettier()
  return {
    exe = "prettier",
    args = {
      "--stdin-filepath",
      vim.fn.fnameescape(vim.api.nvim_buf_get_name(0)),
      "--config /home/alex/kognic/repos/frontend-annotation/.prettierrc"
    },
    stdin = true
  }
end

require("formatter").setup(
  {
    filetype = {
      sql = {
        function()
          return {
            exe = "pg_format",
            args = {},
            stdin = true
          }
        end
      },
      python = {
        function()
          return {
            exe = "black",
            args = {"-q", "-"},
            stdin = true
          }
        end
      },
      sh = {
        function()
          return {
            exe = "shfmt",
            args = {"-i", 2},
            stdin = true
          }
        end
      },
      lua = {
        -- luafmt
        function()
          return {
            exe = "luafmt",
            args = {"--indent-count", 2, "--stdin"},
            stdin = true
          }
        end
      },
      scala = {
        function()
          return {
            exe = "scalafmt",
            stdin = false
          }
        end
      },
      haskell = {
        function()
          return {
            exe = "ormolu",
            stdin = true
          }
        end
      },
      json = {
        function()
          return {
            exe = "jq",
            stdin = true
          }
        end
      },
      go = {
        function()
          return {
            exe = "gofmt",
            stdin = true
          }
        end
      },
      html = {prettier},
      javascript = {prettier},
      typescript = {prettier},
      typescriptreact = {prettier},
      css = {prettier},
      rust = {
        function()
          return {
            exe = "rustfmt",
            stdin = true
          }
        end
      },
      gleam = {
        function()
          return {
            exe = "gleam format",
            stdin = false
          }
        end
      }
    }
  }
)

vim.api.nvim_exec(
  [[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *.lua,*.sh,*.html,*.js,*.ts,*.tsx,*.hs,*.css,*.go,*.rs,*.gleam,*.sql FormatWrite
augroup END
]],
  true
)
