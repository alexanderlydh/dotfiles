local on_attach = function(_, bufnr)
  local nmap = function(keys, func, desc)
    if desc then
      desc = "LSP: " .. desc
    end

    vim.keymap.set("n", keys, func, {buffer = bufnr, desc = desc})
  end
  local imap = function(keys, func, desc)
    if desc then
      desc = "LSP: " .. desc
    end

    vim.keymap.set("i", keys, func, {buffer = bufnr, desc = desc})
  end

  nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
  nmap("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")

  nmap("gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
  nmap("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation")
  nmap("<leader>D", vim.lsp.buf.type_definition, "Type [D]efinition")
  nmap("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")
  nmap("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
  nmap("<leader>fi", require("telescope.builtin").lsp_incoming_calls, "[F]ind [I]ncoming")
  nmap("<leader>fo", require("telescope.builtin").lsp_outgoing_calls, "[F]ind [O]utgoing")
  nmap("<leader>sp", require("telescope.builtin").builtin, "[S]earch [P]icker")

  nmap(
    "<leader>sv",
    function()
      require("telescope.builtin").lsp_definitions({jump_type = "vsplit"})
    end,
    "[G]o [V]split"
  )

  nmap(
    "<leader>ss",
    function()
      require("telescope.builtin").lsp_definitions({jump_type = "split"})
    end,
    "[G]o [S]plit"
  )
  -- See `:help K` for why this keymap
  nmap("K", vim.lsp.buf.hover, "Hover Documentation")
  nmap("<A-k>", vim.lsp.buf.signature_help, "Signature Documentation")
  imap("<A-k>", vim.lsp.buf.signature_help, "Signature Documentation")

  -- Lesser used LSP functionality
  nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
end

-- local client =
--   vim.lsp.start_client {
--   -- cmd = {"/home/alex/random-repos/educationalsp/main"},
--   cmd = {"/home/alex/personal/cheat-sh-lsp/main"},
--   name = "cheat-lsp",
--   on_attach = on_attach
-- }
--
-- if not client then
--   vim.notify("hey client is not working")
--   return
-- end
--
-- vim.api.nvim_create_autocmd(
--   "BufReadPost",
--   {
--     callback = function()
--       vim.lsp.buf_attach_client(0, client)
--     end
--   }
-- )
