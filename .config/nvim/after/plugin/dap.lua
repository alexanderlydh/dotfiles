local ok, dap = pcall(require, "dap")
if not ok then
  return
end

dap.configurations.scala = {
  {
    type = "scala",
    request = "launch",
    name = "RunOrTest",
    metals = {
      runType = "runOrTestFile"
      --args = { "firstArg", "secondArg", "thirdArg" }, -- here just as an example
    }
  },
  {
    type = "scala",
    request = "launch",
    name = "Test Target",
    metals = {
      runType = "testTarget"
    }
  }
}

local dapui = require("dapui")

vim.keymap.set("n", "<F2>", ":lua require'dap'.step_into()<CR>")
vim.keymap.set("n", "<F3>", ":lua require'dap'.step_over()<CR>")
vim.keymap.set("n", "<F4>", ":lua require'dap'.step_out()<CR>")
vim.keymap.set("n", "<F5>", ":lua require'dap'.continue()<CR>")
vim.keymap.set("n", "<F8>", ":lua require'dap'.restart()<CR>")
vim.keymap.set("n", "<F9>", ":lua require'dap'.terminate()<CR>")

vim.keymap.set("n", "<leader>b", ":lua require'dap'.toggle_breakpoint()<CR>")
vim.keymap.set("n", "<leader>B", ":lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>")
vim.keymap.set("n", "<leader>lp", ":lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>")

local function toggle_ui()
  vim.cmd("NoNeckPain")
  dapui.toggle()
end

vim.keymap.set("n", "<leader>dt", toggle_ui)

require("nvim-dap-virtual-text").setup()
dapui.setup {
  controls = {
    element = "repl",
    enabled = false,
    icons = {
      disconnect = "",
      pause = "",
      play = "",
      run_last = "",
      step_back = "",
      step_into = "",
      step_out = "",
      step_over = "",
      terminate = ""
    }
  },
  element_mappings = {},
  expand_lines = false,
  floating = {
    border = "single",
    mappings = {
      close = {"q", "<Esc>"}
    }
  },
  force_buffers = true,
  icons = {
    collapsed = "",
    current_frame = "",
    expanded = ""
  },
  layouts = {
    {
      elements = {
        {
          id = "stacks",
          size = 0.30
        },
        {
          id = "scopes",
          size = 0.50
        },
        {
          id = "breakpoints",
          size = 0.20
        }
      },
      position = "left",
      size = 60
    },
    {
      elements = {
        {
          id = "console",
          size = 1
        }
      },
      position = "bottom",
      size = 20
    }
  },
  mappings = {
    edit = "e",
    expand = {"<CR>", "<2-LeftMouse>"},
    open = "o",
    remove = "d",
    repl = "r",
    toggle = "t"
  },
  render = {
    indent = 1,
    max_value_lines = 100
  }
}

local dap, dapui = require("dap"), require("dapui")
dap.listeners.after.event_initialized["dapui_config"] = function()
  no_neck_pain = require("no-neck-pain")
  if no_neck_pain.state.enabled then
    vim.cmd("NoNeckPain")
  end
  dapui.open()
end
