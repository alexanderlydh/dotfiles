vim.keymap.set("n", "<leader>gg", "<cmd>Git<CR><C-w><S-l>")
vim.keymap.set(
  "n",
  "<leader>gp",
  function()
    vim.cmd("Gwrite")
    MyInput()
    -- print(commit_string)
    -- vim.cmd("Git! push")
  end
)

function MyInput()
  vim.ui.input(
    {
      prompt = "Commit message: ",
      default = "-"
    },
    function(input)
      if input then
        local commit_string = string.format("silent !git commit -m '%s'", input)
        vim.cmd(commit_string)
        vim.cmd("Git! push")
      else
        print("Aborted")
      end
    end
  )
end


