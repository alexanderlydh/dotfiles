vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Install package manager
-- https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath
  }
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(
  {
    "tpope/vim-fugitive",
    "tpope/vim-surround",
    "tpope/vim-rhubarb",
    "tpope/vim-sleuth",
    {
      "neovim/nvim-lspconfig",
      dependencies = {
        {"williamboman/mason.nvim", config = true},
        "williamboman/mason-lspconfig.nvim",
        {"j-hui/fidget.nvim", tag = "legacy", opts = {}},
        "folke/neodev.nvim"
      }
    },
    {
      "kristijanhusak/vim-dadbod-ui",
      dependencies = {
        {"tpope/vim-dadbod", lazy = true},
        {"kristijanhusak/vim-dadbod-completion", ft = {"sql", "mysql", "plsql"}, lazy = true}
      },
      init = function()
        -- Your DBUI configuration
        vim.g.db_ui_execute_on_save = 1
        vim.g.db_ui_auto_execute_table_helpers = 1
        vim.g.db_ui_use_nerd_fonts = 1
      end
    },
    {
      "hrsh7th/nvim-cmp",
      dependencies = {
        "L3MON4D3/LuaSnip",
        "saadparwaiz1/cmp_luasnip",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-cmdline",
        "rafamadriz/friendly-snippets"
      }
    },
    {"mfussenegger/nvim-dap"},
    {"mfussenegger/nvim-dap-python"},
    {"nvim-telescope/telescope-dap.nvim"},
    {
      "stevearc/oil.nvim",
      dependencies = {"nvim-tree/nvim-web-devicons"}
    },
    {
      "danielfalk/smart-open.nvim",
      branch = "0.2.x",
      config = function()
        require("telescope").load_extension("smart_open")
      end,
      dependencies = {
        "kkharji/sqlite.lua",
        -- Only required if using match_algorithm fzf
        {"nvim-telescope/telescope-fzf-native.nvim", build = "make"},
        -- Optional.  If installed, native fzy will be used when match_algorithm is fzy
        {"nvim-telescope/telescope-fzy-native.nvim"}
      }
    },
    {"theHamsta/nvim-dap-virtual-text"},
    {"nvim-neotest/nvim-nio"},
    {"rcarriga/nvim-dap-ui"},
    {"simrat39/rust-tools.nvim"},
    {"echasnovski/mini.cursorword", version = "*"},
    {
      "windwp/nvim-autopairs",
      event = "InsertEnter",
      opts = {
        enable_check_bracket_line = false,
        check_ts = true,
        ignored_next_char = [=[[%w%%%'%[%"%.%`%$]]=]
      } -- this is equalent to setup({}) function
    },
    {"folke/which-key.nvim", opts = {}},
    {
      -- Adds git related signs to the gutter, as well as utilities for managing changes
      "lewis6991/gitsigns.nvim",
      opts = {
        -- See `:help gitsigns.txt`
        signs = {
          add = {text = "+"},
          change = {text = "~"},
          delete = {text = "_"},
          topdelete = {text = "‾"},
          changedelete = {text = "~"}
        },
        on_attach = function(bufnr)
          vim.keymap.set(
            "n",
            "<leader>hp",
            require("gitsigns").preview_hunk,
            {buffer = bufnr, desc = "Preview git hunk"}
          )

          -- don't override the built-in and fugitive keymaps
          local gs = package.loaded.gitsigns
          vim.keymap.set(
            {"n", "v"},
            "]c",
            function()
              if vim.wo.diff then
                return "]c"
              end
              vim.schedule(
                function()
                  gs.next_hunk()
                end
              )
              return "<Ignore>"
            end,
            {expr = true, buffer = bufnr, desc = "Jump to next hunk"}
          )
          vim.keymap.set(
            {"n", "v"},
            "[c",
            function()
              if vim.wo.diff then
                return "[c"
              end
              vim.schedule(
                function()
                  gs.prev_hunk()
                end
              )
              return "<Ignore>"
            end,
            {expr = true, buffer = bufnr, desc = "Jump to previous hunk"}
          )
        end
      }
    },
    {"navarasu/onedark.nvim"},
    {
      "HoNamDuong/hybrid.nvim",
      lazy = false,
      priority = 1000,
      opts = {}
    },
    {
      "rose-pine/neovim",
      name = "rose-pine"
    },
    {"rebelot/kanagawa.nvim"},
    {"folke/tokyonight.nvim"},
    {"aktersnurra/no-clown-fiesta.nvim"},
    {"marko-cerovac/material.nvim"},
    {
      "nvim-lualine/lualine.nvim",
      opts = {
        options = {
          icons_enabled = true,
          component_separators = "|",
          section_separators = ""
        },
        sections = {
          lualine_a = {"mode"},
          lualine_b = {"branch", "diff", "diagnostics"},
          lualine_c = {{"filename", path = 1, shorting_target = 40}},
          lualine_x = {},
          lualine_y = {"progress"},
          lualine_z = {"location"}
        },
        inactive_sections = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = {{"filename", path = 1, shorting_target = 40}},
          lualine_x = {"location"},
          lualine_y = {},
          lualine_z = {}
        }
      }
    },
    {
      "lukas-reineke/indent-blankline.nvim",
      main = "ibl",
      opts = {
        indent = {char = "·"},
        scope = {enabled = false}
      }
    },
    {
      "nvim-telescope/telescope.nvim",
      dependencies = {
        "nvim-lua/plenary.nvim",
        {
          "nvim-telescope/telescope-fzf-native.nvim",
          build = "make",
          cond = function()
            return vim.fn.executable "make" == 1
          end
        }
      }
    },
    {
      "ThePrimeagen/harpoon",
      branch = "harpoon2",
      dependencies = {"nvim-lua/plenary.nvim"}
    },
    {
      -- Highlight, edit, and navigate code
      "nvim-treesitter/nvim-treesitter",
      dependencies = {
        "nvim-treesitter/nvim-treesitter-textobjects"
      },
      build = ":TSUpdate"
    },
    {"m-demare/hlargs.nvim"},
    {"shortcuts/no-neck-pain.nvim"},
    {"mhartington/formatter.nvim"},
    {"aserowy/tmux.nvim"},
    {
      "RishabhRD/nvim-cheat.sh",
      dependencies = {
        "RishabhRD/popfix"
      }
    },
    -- NOTE: Next Step on Your Neovim Journey: Add/Configure additional "plugins" for kickstart
    --       These are some example plugins that I've included in the kickstart repository.
    --       Uncomment any of the lines below to enable them.
    -- require 'kickstart.plugins.autoformat',
    require "kickstart.plugins.debug" --    You can use this folder to prevent any conflicts with this init.lua if you're interested in keeping -- NOTE: The import below can automatically add your own plugins, configuration, etc from `lua/custom/plugins/*.lua`
    --    up-to-date with whatever is in the kickstart repo.
    --    Uncomment the following line and add your plugins to `lua/custom/plugins/*.lua` to get going.
    --
    --    For additional information see: https://github.com/folke/lazy.nvim#-structuring-your-plugins
    -- { import = 'custom.plugins' },
  }
)

require("nvim-cheat")

local tmux = require("tmux")
tmux.setup {
  navigation = {
    enable_default_keybindings = false
  }
}

require("no-neck-pain").setup(
  {
    integrations = {
      NvimDAPUI = {
        -- The position of the tree.
        --- @type "none"
        position = "none",
        -- When `true`, if the tree was opened before enabling the plugin, we will reopen it.
        reopen = true
      }
    }
  }
)

require("oil").setup(
  {
    skip_confirm_for_simple_edits = true,
    columns = {
      "icon"
    },
    view_options = {
      is_hidden_file = function(name, bufnr)
        return vim.startswith(name, ".") or string.find(name, "__pycache__") or string.find(name, ".egg")
      end
    },
    keymaps = {
      ["g?"] = "actions.show_help",
      ["<CR>"] = "actions.select",
      ["<C-v>"] = "actions.select_vsplit",
      ["<C-s>"] = "actions.select_split",
      ["<C-t>"] = "actions.select_tab",
      ["<C-p>"] = "actions.preview",
      ["<C-c>"] = "actions.close",
      ["<C-r>"] = "actions.refresh",
      ["-"] = "actions.parent",
      ["_"] = "actions.open_cwd",
      ["`"] = "actions.cd",
      ["~"] = "actions.tcd",
      ["gs"] = "actions.change_sort",
      ["gx"] = "actions.open_external",
      ["g."] = "actions.toggle_hidden",
      ["g\\"] = "actions.toggle_trash"
    },
    -- Set to false to disable all of the above keymaps
    use_default_keymaps = false
  }
)
local harpoon = require("harpoon")

-- REQUIRED
harpoon:setup()
-- REQUIRED

vim.keymap.set(
  "n",
  "<leader>fa",
  function()
    harpoon:list():add()
  end
)
vim.keymap.set(
  "n",
  "<leader>j",
  function()
    harpoon.ui:toggle_quick_menu(harpoon:list())
  end
)

-- Toggle previous & next buffers stored within Harpoon list
vim.keymap.set(
  "n",
  "<A-S-P>",
  function()
    harpoon:list():prev()
  end
)
vim.keymap.set(
  "n",
  "<A-S-N>",
  function()
    harpoon:list():next()
  end
)

require("mini.cursorword").setup()

require("no-neck-pain").setup(
  {
    width = 150,
    autocmds = {
      enableOnVimEnter = true,
      enableOnTabEnter = true,
      reloadOnColorSchemeChange = true
    },
    buffers = {
      right = {
        enabled = false
      }
    }
  }
)

-- Default options:
require("kanagawa").setup(
  {
    compile = false, -- enable compiling the colorscheme
    undercurl = true, -- enable undercurls
    commentStyle = {italic = true},
    functionStyle = {},
    keywordStyle = {italic = true},
    statementStyle = {bold = true},
    typeStyle = {},
    transparent = false, -- do not set background color
    dimInactive = false, -- dim inactive window `:h hl-NormalNC`
    terminalColors = true, -- define vim.g.terminal_color_{0,17}
    colors = {
      -- add/modify theme and palette colors
      palette = {},
      theme = {wave = {}, lotus = {}, dragon = {}, all = {}}
    },
    overrides = function(colors) -- add/modify highlights
      return {}
    end,
    theme = "wave", -- Load "wave" theme when 'background' option is not set
    background = {
      -- map the value of 'background' option to a theme
      dark = "dragon", -- try "dragon" !
      light = "lotus"
    }
  }
)

vim.o.autoindent = true
vim.o.autoread = true -- if a file is changed outside Vim, automatically re-read it
vim.o.background = "dark"
vim.o.breakindent = true
-- vim.o.clipboard = "unnamedplus"
vim.o.cmdheight = 1
vim.o.completeopt = "menuone,noselect"
vim.o.cursorline = true -- Color the cursorline
vim.o.expandtab = true
vim.o.fillchars = "stl:⸺,stlnc:⸺,vert:│"
vim.o.hidden = true -- Buffer becomes hidden when abandoned to prevent need to save
vim.o.hlsearch = false -- Search highlighting
vim.o.ignorecase = true -- Search ignore case
vim.o.incsearch = true -- Search starts while entering string
vim.o.inccommand = "split"
vim.o.infercase = true -- Adjust case in insert completion mode
vim.o.laststatus = 2 -- Always show the status line
vim.o.lazyredraw = true -- Only redraw when necessary
vim.o.linebreak = true
vim.o.mouse = "a"
vim.o.mousemodel = "popup"
vim.o.number = true
vim.o.pumheight = 30
vim.o.relativenumber = true
vim.o.scrolloff = 5
vim.o.shiftwidth = 4
vim.o.showmatch = true -- Jump to matching bracket
vim.o.signcolumn = "yes:1"
vim.o.smartcase = true -- Search ignore case unless search contains an uppercase
vim.o.softtabstop = 4
vim.o.splitbelow = true -- New splits open to right and bottom
vim.o.splitright = true -- New splits open to right and bottom
vim.o.tabstop = 4
vim.o.termguicolors = true
vim.o.tgc = true
vim.o.timeoutlen = 300
vim.o.undofile = true
vim.o.undolevels = 2000 -- Number of undo levels
vim.o.updatetime = 250
vim.o.wrap = false
vim.o.wrapscan = true -- Searches wrap around the end of the file
vim.o.colorcolumn = ""
vim.wo.number = true
vim.wo.signcolumn = "yes"

-- [[ Basic Keymaps ]]

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({"n", "v"}, "<Space>", "<Nop>", {silent = true})
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

vim.keymap.set("n", "<C-i>", "<C-i>zz")
vim.keymap.set("n", "<C-o>", "<C-o>zz")

vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("n", "<leader>S", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "S", ":%s//g<Left><Left>")

-- delete, yank and paste stuff
vim.keymap.set({"n", "v"}, "<leader>p", [["+p]])
vim.keymap.set({"n", "v"}, "<leader>P", [["+P]])

vim.keymap.set("x", "<leader>rp", [["_d"+p]])
vim.keymap.set("x", "<leader>rP", [["_d"+P]])

vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set({"n", "v"}, "<leader>Y", [["+Y]])

vim.keymap.set({"n", "v"}, "<leader>dd", [["_d]])

vim.keymap.set("t", "<esc>", "<C-\\><C-N>")

-- move panes easier
vim.keymap.set("n", "<C-h>", [[<cmd>lua require("tmux").move_left()<cr>]])
vim.keymap.set("n", "<C-j>", [[<cmd>lua require("tmux").move_bottom()<cr>]])
vim.keymap.set("n", "<C-k>", [[<cmd>lua require("tmux").move_top()<cr>]])
vim.keymap.set("n", "<C-l>", [[<cmd>lua require("tmux").move_right()<cr>]])

--Fill Screen vertically
vim.keymap.set("n", "<Home>", [[<C-w>_]])

--Fill Screen horizontally
vim.keymap.set("n", "<End>", [[<C-w>|]])

--For vertical windows, resize windows horizontally using + and -
vim.keymap.set("n", "<PageUp>", "<cmd>vert res +5<cr>")
vim.keymap.set("n", "<PageDown>", "<cmd>vert res -5<cr>")

--For horizontal windows, resize windows vertically using PgUp and PgDn
vim.keymap.set("n", "<C-PageUp>", "<cmd>res +5<cr>")
vim.keymap.set("n", "<C-PageDown>", "<cmd>res -5<cr>")

-- indentation for selection
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", {expr = true, silent = true})
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", {expr = true, silent = true})

vim.keymap.set("n", "-", "<CMD>Oil<CR>", {desc = "Open parent directory"})

vim.keymap.set("n", "<leader>n", "<CMD>NoNeckPain<CR>", {desc = "Toggle NoNeckPain"})
vim.keymap.set("n", "<leader>fm", "<CMD>Format<CR>", {desc = "Format file"})

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup("YankHighlight", {clear = true})
vim.api.nvim_create_autocmd(
  "TextYankPost",
  {
    callback = function()
      vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = "*"
  }
)

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
local telescope = require("telescope")
telescope.setup {
  defaults = {
    layout_strategy = "horizontal",
    layout_config = {width = 0.6, height = 0.95},
    path_display = {"smart"}
  },
  pickers = {
    lsp_references = {
      show_line = false
    }
  }
}

-- Enable telescope fzf native, if installed
pcall(require("telescope").load_extension, "fzf")

local telescope = require("telescope.builtin")

-- See `:help telescope.builtin`
vim.keymap.set("n", "<leader>?", require("telescope.builtin").oldfiles, {desc = "[?] Find recently opened files"})
vim.keymap.set(
  "n",
  "<leader>ff",
  function()
    -- You can pass additional configuration to telescope to change theme, layout, etc.
    require("telescope.builtin").current_buffer_fuzzy_find(
      require("telescope.themes").get_dropdown {
        winblend = 10,
        previewer = false
      }
    )
  end,
  {desc = "[/] Fuzzily search in current buffer"}
)

vim.keymap.set(
  "n",
  "<leader><leader>",
  function()
    require("telescope").extensions.smart_open.smart_open {layout_strategy = "vertical"}
  end,
  {desc = "[F]ind [F]iles"}
)
vim.keymap.set("n", "<leader>sh", require("telescope.builtin").help_tags, {desc = "[S]earch [H]elp"})
vim.keymap.set("n", "<leader>sw", require("telescope.builtin").grep_string, {desc = "[S]earch current [W]ord"})

local function find_exact_word()
  local opts = {word_match = "-w"}
  telescope.grep_string(opts)
end

vim.keymap.set("n", "<leader>se", find_exact_word, {desc = "[S]earch [E]xact current word"})
vim.keymap.set("n", "<leader>fg", require("telescope.builtin").live_grep, {desc = "[F]ind by [G]rep"})
vim.keymap.set("n", "<leader>fd", require("telescope.builtin").diagnostics, {desc = "[F]ind [D]iagnostics"})
vim.keymap.set("n", "<leader>fr", require("telescope.builtin").resume, {desc = "[F]ind [R]resume"})
vim.keymap.set("n", "<leader>fu", require("telescope.builtin").lsp_references, {desc = "[F]ind usage"})

require("nvim-treesitter.configs").setup {
  ensure_installed = {"python", "rust", "scala"},
  additional_vim_regex_highlighting = false,
  auto_install = true,
  highlight = {enable = true, disable = {"json"}},
  indent = {enable = true},
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "<c-space>",
      node_incremental = "<c-space>",
      scope_incremental = "<c-s>",
      node_decremental = "<M-space>"
    }
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["a="] = {query = "@assignment.outer", desc = "Select outer part of an assignment"},
        ["i="] = {query = "@assignment.inner", desc = "Select inner part of an assignment"},
        --
        ["aa"] = {query = "@parameter.outer", desc = "Select outer part of a parameter/argument"},
        ["ia"] = {query = "@parameter.inner", desc = "Select inner part of a parameter/argument"},
        --
        ["ai"] = {query = "@conditional.outer", desc = "Select outer part of a conditional"},
        ["ii"] = {query = "@conditional.inner", desc = "Select inner part of a conditional"},
        --
        ["al"] = {query = "@loop.outer", desc = "Select outer part of a loop"},
        ["il"] = {query = "@loop.inner", desc = "Select inner part of a loop"},
        --
        ["af"] = {query = "@call.outer", desc = "Select outer part of a function call"},
        ["if"] = {query = "@call.inner", desc = "Select inner part of a function call"},
        --
        ["am"] = {query = "@function.outer", desc = "Select outer part of a method/function definition"},
        ["im"] = {query = "@function.inner", desc = "Select inner part of a method/function definition"},
        --
        ["ac"] = {query = "@class.outer", desc = "Select outer part of a class"},
        ["ic"] = {query = "@class.inner", desc = "Select inner part of a class"}
      }
    },
    move = {
      enable = true,
      set_jumps = true -- whether to set jumps in the jumplist
    },
    swap = {
      enable = true,
      swap_next = {
        ["<leader>a"] = "@parameter.inner"
      },
      swap_previous = {
        ["<leader>A"] = "@parameter.inner"
      }
    }
  }
}

vim.keymap.set("n", "<A-i>", "<CMD>TSTextobjectGotoNextStart @function.outer<CR>zz")
vim.keymap.set("n", "<A-o>", "<CMD>TSTextobjectGotoPreviousStart @function.outer<CR>zz")

vim.keymap.set("n", "<C-n>", "<CMD>TSTextobjectGotoNextStart @class.outer<CR>zz")
vim.keymap.set("n", "<C-p>", "<CMD>TSTextobjectGotoPreviousStart @class.outer<CR>zz")

require("hlargs").setup()

-- Diagnostic keymaps
vim.keymap.set("n", "<A-O>", vim.diagnostic.goto_prev, {desc = "Go to previous diagnostic message"})
vim.keymap.set("n", "<A-I>", vim.diagnostic.goto_next, {desc = "Go to next diagnostic message"})
vim.keymap.set("n", "<leader>de", vim.diagnostic.open_float, {desc = "Open floating diagnostic message"})
vim.keymap.set("n", "<leader>dq", vim.diagnostic.setloclist, {desc = "Open diagnostics list"})
vim.keymap.set(
  "n",
  "<leader>dh",
  function()
    vim.diagnostic.config(
      {
        virtual_text = false
      }
    )
  end,
  {desc = "[D]iagnostics [H]ide"}
)
vim.keymap.set(
  "n",
  "<leader>ds",
  function()
    vim.diagnostic.config(
      {
        virtual_text = true
      }
    )
  end,
  {desc = "[D]iagnostics [S]how"}
)

vim.diagnostic.config(
  {
    virtual_text = false
  }
)

-- [[ Configure LSP ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(client, bufnr)
  local nmap = function(keys, func, desc)
    if desc then
      desc = "LSP: " .. desc
    end

    vim.keymap.set("n", keys, func, {buffer = bufnr, desc = desc})
  end
  local imap = function(keys, func, desc)
    if desc then
      desc = "LSP: " .. desc
    end

    vim.keymap.set("i", keys, func, {buffer = bufnr, desc = desc})
  end

  nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
  nmap("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")
  nmap("<leader>lr", "<CMD>e<CR>", "[L]sp [Restart]")

  nmap("gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
  nmap("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation")
  nmap("<leader>D", vim.lsp.buf.type_definition, "Type [D]efinition")
  nmap("<leader>fs", require("telescope.builtin").lsp_document_symbols, "[F]ing [S]ymbols")
  nmap("<leader>fw", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[F]ind [W]orkspace symbols")
  nmap("<leader>fi", require("telescope.builtin").lsp_incoming_calls, "[F]ind [I]ncoming")
  nmap("<leader>fo", require("telescope.builtin").lsp_outgoing_calls, "[F]ind [O]utgoing")
  nmap("<leader>sp", require("telescope.builtin").builtin, "[S]earch [P]icker")

  nmap(
    "<leader>sv",
    function()
      require("telescope.builtin").lsp_definitions({jump_type = "vsplit"})
    end,
    "[G]o [V]split"
  )

  nmap(
    "<leader>ss",
    function()
      require("telescope.builtin").lsp_definitions({jump_type = "split"})
    end,
    "[G]o [S]plit"
  )
  -- See `:help K` for why this keymap
  nmap("K", vim.lsp.buf.hover, "Hover Documentation")
  nmap("<A-k>", vim.lsp.buf.signature_help, "Signature Documentation")
  imap("<A-k>", vim.lsp.buf.signature_help, "Signature Documentation")

  -- Lesser used LSP functionality
  nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
end

local servers = {
  tsserver = {},
  -- html = { filetypes = { 'html', 'twig', 'hbs'} },

  lua_ls = {
    Lua = {
      workspace = {checkThirdParty = false},
      telemetry = {enable = false},
      diagnostics = {disable = {"missing-fields"}}
    }
  }
}
local rt = require("rust-tools")

rt.setup(
  {
    server = {
      on_attach = function(client, bufnr)
        on_attach(client, bufnr)
        -- Hover actions
        vim.keymap.set("n", "K", rt.hover_actions.hover_actions, {buffer = bufnr})
        -- Code action groups
        vim.keymap.set("n", "<Leader>ca", rt.code_action_group.code_action_group, {buffer = bufnr})
      end,
      settings = {
        ["rust-analyzer"] = {
          checkOnSave = {
            enable = true,
            command = "clippy"
          },
          cargo = {
            allFeatures = true
          }
        }
      }
    },
    -- debugging stuff
    dap = {
      adapter = {
        type = "executable",
        command = "lldb-vscode",
        name = "rt_lldb"
      }
    },
    tools = {
      -- These apply to the default RustSetInlayHints command
      inlay_hints = {
        -- automatically set inlay hints (type hints)
        -- default: true
        auto = true,
        -- Only show inlay hints for the current line
        only_current_line = false,
        -- whether to show parameter hints with the inlay hints or not
        -- default: true
        show_parameter_hints = true,
        -- prefix for parameter hints
        -- default: "<-"
        parameter_hints_prefix = "",
        -- prefix for all the other hints (type, chaining)
        -- default: "=>"
        other_hints_prefix = "=> ",
        -- whether to align to the length of the longest line in the file
        max_len_align = true,
        -- padding from the left if max_len_align is true
        max_len_align_padding = 1,
        -- whether to align to the extreme right or not
        right_align = false,
        -- padding from the right if right_align is true
        right_align_padding = 7,
        -- The color of the hints
        highlight = "Comment"
      }
    }
  }
)

-- Setup neovim lua configuration
require("neodev").setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require "mason-lspconfig"

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers)
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require("lspconfig")[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
      filetypes = (servers[server_name] or {}).filetypes
    }
  end
}

local lspconfig = require("lspconfig")
lspconfig.gleam.setup {
  capabilities = capabilities,
  on_attach = on_attach
}

lspconfig.gopls.setup {
  capabilities = capabilities,
  on_attach = on_attach
}

-- lspconfig.pylsp.setup {
--   on_attach = on_attach,
--   capabilities = capabilities
-- }

lspconfig.pyright.setup {
  capabilities = capabilities,
  on_attach = on_attach,
  settings = {
    basedpyright = {
      analysis = {
        autoSearchPaths = true,
        diagnosticMode = "workspace",
        useLibraryCodeForTypes = true,
        deprecateTypingAliases = false,
        diagnosticSeverityOverrides = {
          reportAny = "none",
          reportMissingTypeStubs = "none",
          reportUnknownArgumentType = "none",
          reportUnknownLambdaType = "none",
          reportUnknownMemberType = "none",
          reportUnknownParameterType = "none",
          reportUnknownVariableType = "none",
          reportDeprecated = false,
          deprecateTypingAliases = false,
          reportDeprecateTypingAliases = false
        }
      }
    }
  }
}

-- [[ Configure nvim-cmp ]]
-- See `:help cmp`
local cmp = require "cmp"
local luasnip = require "luasnip"
require("luasnip.loaders.from_vscode").lazy_load({paths = {"./snippets"}})
require("luasnip.loaders.from_vscode").lazy_load()
luasnip.config.setup {}

local cmp_autopairs = require("nvim-autopairs.completion.cmp")

cmp.setup {
  formatting = {
    fields = {"abbr", "menu", "kind"},
    format = function(entry, item)
      -- Define menu shorthand for different completion sources.
      local menu_icon = {
        nvim_lsp = "NLSP",
        nvim_lua = "NLUA",
        buffer = "BUFF",
        path = "PATH"
      }
      -- Set the menu "icon" to the shorthand for each completion source.
      item.menu = menu_icon[entry.source.name]

      -- Set the fixed width of the completion menu to 60 characters.
      -- fixed_width = 20

      -- Set 'fixed_width' to false if not provided.
      fixed_width = fixed_width or false

      -- Get the completion entry text shown in the completion window.
      local content = item.abbr

      -- Set the fixed completion window width.
      if fixed_width then
        vim.o.pumwidth = fixed_width
      end

      -- Get the width of the current window.
      local win_width = vim.api.nvim_win_get_width(0)

      -- Set the max content width based on either: 'fixed_width'
      -- or a percentage of the window width, in this case 40%.
      -- We subtract 10 from 'fixed_width' to leave room for 'kind' fields.
      local max_content_width = fixed_width and fixed_width - 10 or math.floor(win_width * 0.4)

      -- Truncate the completion entry text if it's longer than the
      -- max content width. We subtract 3 from the max content width
      -- to account for the "..." that will be appended to it.
      if #content > max_content_width then
        item.abbr = vim.fn.strcharpart(content, 0, max_content_width - 3) .. "..."
      else
        item.abbr = content .. (" "):rep(max_content_width - #content)
      end
      return item
    end
  },
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end
  },
  completion = {
    completeopt = "menu,menuone,noinsert"
  },
  mapping = cmp.mapping.preset.insert {
    ["<C-n>"] = cmp.mapping.select_next_item(),
    ["<C-p>"] = cmp.mapping.select_prev_item(),
    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-e>"] = cmp.mapping.abort(),
    ["<C-Space>"] = cmp.mapping.complete {},
    ["<CR>"] = cmp.mapping(
      function(fallback)
        if cmp.visible() then
          if luasnip.expandable() then
            luasnip.expand()
          else
            cmp.confirm(
              {
                select = true
              }
            )
          end
        else
          fallback()
        end
      end
    ),
    ["<Tab>"] = cmp.mapping(
      function(fallback)
        if luasnip.locally_jumpable(1) then
          luasnip.jump(1)
        elseif cmp.visible() then
          cmp.select_next_item()
        else
          fallback()
        end
      end,
      {"i", "s"}
    ),
    ["<S-Tab>"] = cmp.mapping(
      function(fallback)
        if luasnip.locally_jumpable(-1) then
          luasnip.jump(-1)
        elseif cmp.visible() then
          cmp.select_prev_item()
        else
          fallback()
        end
      end,
      {"i", "s"}
    )
  },
  sources = {
    {name = "nvim_lsp"},
    {name = "buffer"},
    {name = "path"},
    {name = "luasnip"}
  }
}

-- cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
cmp.setup.cmdline(
  "/",
  {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      {name = "buffer"}
    }
  }
)

cmp.setup.cmdline(
  ":",
  {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources(
      {
        {name = "path"}
      },
      {
        {
          name = "cmdline",
          option = {
            ignore_cmds = {"Man", "!"}
          }
        }
      }
    )
  }
)

-- Setup up vim-dadbod
cmp.setup.filetype(
  {"sql"},
  {
    sources = {
      {name = "vim-dadbod-completion"},
      {name = "buffer"}
    }
  }
)

vim.cmd("colorscheme hybrid")

-- open file in last edit position
vim.api.nvim_create_autocmd(
  "BufReadPost",
  {
    desc = "Open file at the last position it was edited earlier",
    pattern = "*",
    command = 'silent! normal! g`"zv'
  }
)

vim.cmd(
  [[
cnoreabbrev W! w!
cnoreabbrev W1 w!
cnoreabbrev w1 w!
cnoreabbrev Q! q!
cnoreabbrev Q1 q!
cnoreabbrev q1 q!
cnoreabbrev Qa! qa!
cnoreabbrev Qall! qall!
cnoreabbrev Wa wa
cnoreabbrev Wq wq
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev wq1 wq!
cnoreabbrev Wq1 wq!
cnoreabbrev wQ1 wq!
cnoreabbrev WQ1 wq!
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qa qa
cnoreabbrev Qall qall
]]
)
